﻿using System;
using System.Collections.Generic;

namespace ExampleAPI.Models
{
    public partial class Position
    {
        public string SecName { get; set; }
        public string SecType { get; set; }
        public string Issuer { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Ccy { get; set; }
        public string Ticker { get; set; }
        public decimal? PriceMultiplier { get; set; }
        public decimal? IssuePrice { get; set; }
        public decimal? LastPrice { get; set; }
        public string RatingSp { get; set; }
        public string RatingMoodys { get; set; }
        public string Cusip { get; set; }
        public string AssetType { get; set; }
        public string RiskCountry { get; set; }
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
