﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ExampleAPI.Models
{
    public partial class CoreAPIContext : DbContext
    {
        public CoreAPIContext()
        {
        }

        public CoreAPIContext(DbContextOptions<CoreAPIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Position> Position { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            //    optionsBuilder.UseSqlServer("Server=localhost;Database=CoreAPI;Trusted_Connection=True;");
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Position>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AssetType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ccy)
                    .HasColumnName("CCY")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Cusip)
                    .HasColumnName("CUSIP")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IssuePrice).HasColumnType("decimal(28, 4)");

                entity.Property(e => e.Issuer)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LastPrice).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.PriceMultiplier).HasColumnType("decimal(28, 4)");

                entity.Property(e => e.RatingMoodys)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RatingSp)
                    .HasColumnName("RatingSP")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RiskCountry)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SecName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SecType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Ticker)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
        }
    }
}
