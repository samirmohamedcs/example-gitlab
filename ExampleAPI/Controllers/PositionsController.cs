﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ExampleAPI.Models;
using ExampleAPI.DTOs;
using ExampleAPI.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace ExampleAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionsController : ControllerBase
    {
        private readonly IPositionCRUDRepo _CRUDRepo;
        private readonly IPositionDetailRepo _detailRepo;
        private readonly ILogger _logger;

        public PositionsController(IPositionCRUDRepo CRUDRepo, IPositionDetailRepo detailRepo, ILogger<PositionsController> logger)
        {
            _CRUDRepo = CRUDRepo;
            _detailRepo = detailRepo;
            _logger = logger;
            _logger.LogDebug("PositionsController created");
        }

        // GET: api/Positions
        [HttpGet]
        public async Task<ActionResult<List<Position>>> GetPositions()
        {
            _logger.LogInformation("GetPositions() returning a list of all positions");
            try
            {
                return await _CRUDRepo.GetPositions();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "GetPositions() exception occured when trying to return a list of all positions");
                return BadRequest();
            }
        }

        // GET: api/Positions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Position>> GetPosition(int id)
        {
            _logger.LogInformation("GetPositions({id}) returning a position", id);
            try
            {
                var position = await _CRUDRepo.GetPosition(id);

                if (position == null)
                {
                    _logger.LogWarning("GetPosition({ID}) NOT FOUND", id);
                    return NotFound();
                }

                return position;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "GetPosition({id}) exception occured when trying to return a position", id);
                return BadRequest();
            }
        }

        // PUT: api/Positions
        [HttpPut]
        public async Task<ActionResult<Position>> PutPosition(Position position)
        {
            _logger.LogInformation("PutPosition(position) updating position with SecName = \"{SecName}\" ", position.SecName);
            try
            {
                int result = await _CRUDRepo.UpdatePosition(position);
                if(result == 0)
                {
                    _logger.LogWarning("PutPosition(position) position with SecName = \"{SecName}\" NOT FOUND", position.SecName);
                    return NotFound();
                }
                else
                {
                    return position;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "PutPosition(position) exception occured when trying to update position: {positionJson} ", JsonConvert.SerializeObject(position));
                return BadRequest();
            }
        }

        // POST: api/Positions
        [HttpPost]
        public async Task<ActionResult<Position>> PostPosition(Position position)
        {
            _logger.LogInformation("PostPosition(position) creating position with SecName = \"{SecName}\" ", position.SecName);
            try
            {
                int result = await _CRUDRepo.CreatePosition(position);
                if (result == 0)
                {
                    _logger.LogWarning("PutPosition(position) position with SecName = \"{SecName}\" NOT FOUND", position.SecName);
                    return UnprocessableEntity();
                }
                else
                {
                    return CreatedAtAction("GetPositions", new { id = position.Id }, position);
                }
            }
            catch(Exception e)
            {
                _logger.LogWarning(e, "PostPosition(position) exception occured when trying to create position: {positionJson} ", JsonConvert.SerializeObject(position));
                return BadRequest();
            }
        }

        // DELETE: api/Positions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Position>> DeletePosition(int id)
        {
            _logger.LogInformation("DeletePosition({id}) deleting a position", id);
            try
            {
                Position updatedPosition = await _CRUDRepo.DeletePosition(id);
                if (updatedPosition == null)
                {
                    _logger.LogWarning("DeletePosition({ID}) NOT FOUND", id);
                    return NotFound();
                }
                else
                {
                    return updatedPosition;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeletePosition({id}) exception occured when trying to delete position ", id);
                return BadRequest();
            }
        }


        // GET: api/Positions/GetAveragePriceBySecType
        [HttpGet("GetAveragePriceBySecType")]
        public async Task<ActionResult<List<SecType_AveragePrice>>> GetAveragePriceBySecType()
        {
            _logger.LogInformation("GetAveragePriceBySecType() returning a list of SecTypes and their Average Prices");
            try
            {
                return await _detailRepo.GetAveragePriceBySecType();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetAveragePriceBySecType() exception occured when trying to return a list of SecTypes and their Average Prices");
                return BadRequest();
            }
        }

        [HttpGet("GetNonInvestmentGradePositions")]
        public async Task<ActionResult<List<Position>>> GetNonInvestmentGradePositions()
        {
            _logger.LogInformation("GetNonInvestmentGradePositions() returning a list of positions that are not investment grade");
            try
            {
                return await _detailRepo.GetNonInvestmentGradePositions();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetNonInvestmentGradePositions() exception occured when trying to return a list of positions that are not investment grade");
                return BadRequest();
            }
        }

        [HttpGet("GetPositionsByRiskCountry/{country}")]
        public async Task<ActionResult<List<Position>>> GetPositionsByRiskCountry(string country)
        {
            _logger.LogInformation("GetPositionsByRiskCountry({string country}) returning a list of positions by risk country", country);
            try
            {
                var positions = await _detailRepo.GetPositionsByRiskCountry(country);

                if (positions == null)
                {
                    _logger.LogWarning("GetPositionsByRiskCountry({string country}) NOT FOUND", country);
                    return NotFound();
                }

                return positions;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetPositionsByRiskCountry({string country}) exception occured when trying to return a list of positions by risk country", country);
                return BadRequest();
            }
        }

    }
}
