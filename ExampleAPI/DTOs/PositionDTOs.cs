﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleAPI.DTOs
{
    public class SecType_AveragePrice
    {
        public string SecType { get; set; }
        public decimal? AveragePrice { get; set; }
    }
}
