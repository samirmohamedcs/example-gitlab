﻿using ExampleAPI.Interfaces;
using ExampleAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleAPI.Repositories
{
    public class PositionCRUDRepo : IPositionCRUDRepo
    {
        private readonly CoreAPIContext _context;
        public PositionCRUDRepo(CoreAPIContext context)
        {
            _context = context;
        }
        
        public async Task<int> CreatePosition(Position position)
        {
            Position existingPosition = await _context.Position.SingleOrDefaultAsync(p => p.SecName == position.SecName);
            if (existingPosition == null)
            {
                _context.Position.Add(position);
                return await _context.SaveChangesAsync();
            }
            else return 0;
        }

        public Task<Position> GetPosition(int id)
        {
            return _context.Position.FindAsync(id);
        }

        public Task<List<Position>> GetPositions()
        {
            return _context.Position.ToListAsync(); 
        }
        
        public async Task<int> UpdatePosition(Position updatedPosition)
        {
            try
            {
                Position existingPosition = await _context.Position.SingleOrDefaultAsync(p => p.SecName == updatedPosition.SecName);
                if(existingPosition == null)
                {
                    return 0;
                }
                updatedPosition.Id = existingPosition.Id;
                _context.Entry(existingPosition).State = EntityState.Detached;
                _context.Entry(updatedPosition).State = EntityState.Modified;
                return await _context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<Position> DeletePosition(int id)
        {
            Position existingPosition = await GetPosition(id);
            if (existingPosition == null)
            {
                return null;
            }
            _context.Position.Remove(existingPosition);
            await _context.SaveChangesAsync();
            return existingPosition;
        }
    }
}
