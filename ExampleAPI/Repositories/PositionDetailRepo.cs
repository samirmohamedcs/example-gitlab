﻿using ExampleAPI.DTOs;
using ExampleAPI.Interfaces;
using ExampleAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleAPI.Repositories
{
    public class PositionDetailRepo : IPositionDetailRepo
    {
        private readonly CoreAPIContext _context;
        public PositionDetailRepo(CoreAPIContext context)
        {
            _context = context;
        }
        public Task<List<SecType_AveragePrice>> GetAveragePriceBySecType()
        {
            return _context.Position
                .GroupBy(st => new { st.SecType })
                .Select(sa => new SecType_AveragePrice
                {
                    SecType = sa.Key.SecType,
                    AveragePrice = sa.Average(st => st.LastPrice)
                }).ToListAsync();
        }

        public Task<List<Position>> GetNonInvestmentGradePositions()
        {
            return _context.Position
                .Where(p =>
                    (p.RatingSp != "AAA" && p.RatingSp != "AA+" && p.RatingSp != "AA" && p.RatingSp != "AA-" && p.RatingSp != "")
                    || (p.RatingMoodys != "Aaa" && p.RatingMoodys != "Aa1" && p.RatingMoodys != "Aa2" && p.RatingMoodys != "Aa3" 
                        && p.RatingMoodys != "A1" && p.RatingMoodys != "A2" && p.RatingMoodys != "A3" && p.RatingMoodys != "Baa1" 
                        && p.RatingMoodys != "Baa2" && p.RatingMoodys != "Baa3" && p.RatingMoodys != "")
                )
            .ToListAsync();
        }

        public Task<List<Position>> GetPositionsByRiskCountry(string country)
        {
            return _context.Position
                    .Where(p => p.RiskCountry == country)
                    .ToListAsync();
        }

        //public Task<List<string>> GetDistinctValuesByColumn(string columnName)
        //{
        //    return _context.Position.Select(p => p.)
        //}
    }
}
