﻿using ExampleAPI.Models;
using ExampleAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleAPI.Interfaces
{
    public interface IPositionDetailRepo
    {
        Task<List<Position>> GetPositionsByRiskCountry(string country);
        Task<List<SecType_AveragePrice>> GetAveragePriceBySecType();
        Task<List<Position>> GetNonInvestmentGradePositions();
    }
}
