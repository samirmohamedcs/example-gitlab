﻿using ExampleAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleAPI.Interfaces
{
    public interface IPositionCRUDRepo
    {
        Task<List<Position>> GetPositions();
        Task<Position> GetPosition(int id);
        Task<int> UpdatePosition(Position position);
        Task<int> CreatePosition(Position position);
        Task<Position> DeletePosition(int id);
    }
}
