﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ExampleAPI.Migrations
{
    public partial class ChangedPositionsToPosition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Positions");

            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SecName = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    SecType = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Issuer = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CCY = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    Ticker = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    PriceMultiplier = table.Column<decimal>(type: "decimal(28, 4)", nullable: true),
                    IssuePrice = table.Column<decimal>(type: "decimal(28, 4)", nullable: true),
                    LastPrice = table.Column<decimal>(type: "decimal(28, 6)", nullable: true),
                    RatingSP = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    RatingMoodys = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    CUSIP = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    AssetType = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    RiskCountry = table.Column<string>(unicode: false, maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Position", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Position");

            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssetType = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CCY = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CUSIP = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IssuePrice = table.Column<decimal>(type: "decimal(28, 4)", nullable: true),
                    Issuer = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    LastPrice = table.Column<decimal>(type: "decimal(28, 6)", nullable: true),
                    PriceMultiplier = table.Column<decimal>(type: "decimal(28, 4)", nullable: true),
                    RatingMoodys = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    RatingSP = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    RiskCountry = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    SecName = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    SecType = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Ticker = table.Column<string>(unicode: false, maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.ID);
                });
        }
    }
}
