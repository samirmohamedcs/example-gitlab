﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using ExampleAPI.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using ExampleAPI.Interfaces;
using Microsoft.Extensions.Logging;
using ExampleAPI.DTOs;
using ExampleAPI.Repositories;

namespace ExampleAPI.Tests
{
    public class PositionCRUDRepo_Test
    {
        public DbContextOptions<CoreAPIContext> GetTestDBContextOptions()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            DbContextOptions<CoreAPIContext> options = new DbContextOptionsBuilder<CoreAPIContext>()
                .UseSqlite(connection)
                .Options;
            return options;
        }

        public Position TestPosition1()
        {
            return new Position { SecName = "Example1", Id = 1, LastPrice = 1, CreatedAt = DateTime.Now };
        }
        public Position TestPosition2()
        {
            return new Position { SecName = "Example2", Id = 2, LastPrice = 1, CreatedAt = DateTime.Now };
        }
        public Position TestPosition3()
        {
            return new Position { SecName = "Example3", Id = 3, LastPrice = 1, CreatedAt = DateTime.Now };
        }
        public Position TestPositionNoId()
        {
            return new Position { SecName = "Example1", LastPrice = 1, CreatedAt = DateTime.Now };
        }

        [Fact]
        public async Task UpdatePosition_UpdatesLastPrice()
        {
            int newLastPrice = 2;
            //Arrange - set dbcontext to in memory sql lite
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - insert a new position
            Position originalPosition = TestPosition1();
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(originalPosition);
                context.SaveChanges();
            }

            //Arrange - Update previous position
            Position updatedPosition = new Position();
            updatedPosition.SecName = originalPosition.SecName;
            updatedPosition.CreatedAt = DateTime.Now;
            updatedPosition.LastPrice = newLastPrice;
            PositionCRUDRepo repo;
            int result;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.UpdatePosition(updatedPosition);
            }

            //Assert
            using (var context = new CoreAPIContext(options))
            {
                Assert.Equal(context.Position.Find(originalPosition.Id).LastPrice, newLastPrice);
            }
        }

        [Fact]
        public async Task UpdatePosition_Returns1()
        {
            int newLastPrice = 2;
            //Arrange - set dbcontext to in memory sql lite
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - insert a new position
            Position originalPosition = TestPosition1();
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(originalPosition);
                context.SaveChanges();
            }

            //Arrange - Update previous position
            Position updatedPosition = new Position();
            updatedPosition.SecName = originalPosition.SecName;
            updatedPosition.CreatedAt = DateTime.Now;
            updatedPosition.LastPrice = newLastPrice;
            PositionCRUDRepo repo;
            int result;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.UpdatePosition(updatedPosition);
            }

            //Assert
            using (var context = new CoreAPIContext(options))
            {
                Assert.Equal(1, result);
            }
        }

        [Fact]
        public async Task UpdatePosition_NoMatch_Returns0()
        {
            int newLastPrice = 2;
            //Arrange - set dbcontext to in memory sql lite
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - attempt to update position
            Position updatedPosition = TestPositionNoId();
            PositionCRUDRepo repo;
            int result;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.UpdatePosition(updatedPosition);
            }

            //Assert
            using (var context = new CoreAPIContext(options))
            {
                Assert.Equal(0, result);
            }
        }

        [Fact]
        public async Task GetPositions_ReturnsAListOfPositions()
        {
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - insert new positions
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(TestPosition1());
                context.Position.Add(TestPosition2());
                context.Position.Add(TestPosition3());
                context.SaveChanges();
            }

            List<Position> resultList;
            PositionCRUDRepo repo;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                resultList = await repo.GetPositions();
            }

            //Assert
            Assert.Equal(3, resultList.Count);
        }

        [Fact]
        public async Task GetPositions_ReturnsEmptyList()
        {
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }
            
            List<Position> resultList;
            PositionCRUDRepo repo;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                resultList = await repo.GetPositions();
            }

            //Assert
            Assert.Empty(resultList);
        }

        [Fact]
        public async Task GetPosition_ReturnsAPosition()
        {
            int id = 1;
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - insert new positions
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(TestPosition1());
                context.Position.Add(TestPosition2());
                context.Position.Add(TestPosition3());
                context.SaveChanges();
            }

            Position result;
            PositionCRUDRepo repo;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.GetPosition(id);
            }

            //Assert
            Assert.Equal(TestPosition1().SecName, result.SecName);
        }

        [Fact]
        public async Task GetPosition_ReturnsNull()
        {
            int id = 1;
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            Position result;
            PositionCRUDRepo repo;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.GetPosition(1);
            }

            //Assert
            Assert.Null(result);
        }


        //Create Position
        [Fact]
        public async Task CreatePosition_CreatesLastPrice()
        {
            //Arrange - set dbcontext to in memory sql lite
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - Create position
            Position createdPosition = TestPositionNoId();
            PositionCRUDRepo repo;
            int result;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.CreatePosition(createdPosition);
            }

            //Assert
            using (var context = new CoreAPIContext(options))
            {
                Assert.NotEqual(0, createdPosition.Id);
            }
        }

        [Fact]
        public async Task CreatePosition_Returns1()
        {
            //Arrange - set dbcontext to in memory sql lite
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - Create position
            Position createdPosition = TestPositionNoId();
            PositionCRUDRepo repo;
            int result;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.CreatePosition(createdPosition);
            }

            //Assert
            using (var context = new CoreAPIContext(options))
            {
                Assert.Equal(1, result);
            }
        }

        [Fact]
        public async Task CreatePosition_AlreadyExists_Returns0()
        {
            //Arrange - set dbcontext to in memory sql lite
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }
            //Arrange - insert a new position
            Position originalPosition = TestPosition1();
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(originalPosition);
                context.SaveChanges();
            }

            //Arrange - Create same position
            Position createdPosition = TestPositionNoId();
            PositionCRUDRepo repo;
            int result;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.CreatePosition(createdPosition);
            }

            //Assert
            using (var context = new CoreAPIContext(options))
            {
                Assert.Equal(0, result);
            }
        }

        //Delete Position
        [Fact]
        public async Task DeletePosition_ReturnsAPosition()
        {
            int id = 1;
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - insert new positions
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(TestPosition1());
                context.Position.Add(TestPosition2());
                context.Position.Add(TestPosition3());
                context.SaveChanges();
            }

            Position result;
            PositionCRUDRepo repo;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.DeletePosition(id);
            }

            //Assert
            Assert.Equal(TestPosition1().SecName, result.SecName);
        }

        [Fact]
        public async Task DeletePosition_DeletedPosition()
        {
            int id = 1;
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            //Arrange - insert new positions
            using (var context = new CoreAPIContext(options))
            {
                context.Position.Add(TestPosition1());
                context.Position.Add(TestPosition2());
                context.Position.Add(TestPosition3());
                context.SaveChanges();
            }

            Position result;
            PositionCRUDRepo repo;
            int numPositions;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.DeletePosition(id);
                numPositions = context.Position.Count();
            }

            //Assert
            Assert.Equal(2, numPositions);
        }

        [Fact]
        public async Task DeletePosition_ReturnsNull()
        {
            int id = 1;
            //Arrange
            DbContextOptions<CoreAPIContext> options = GetTestDBContextOptions();

            using (var context = new CoreAPIContext(options))
            {
                context.Database.EnsureCreated();
            }

            Position result;
            PositionCRUDRepo repo;
            using (var context = new CoreAPIContext(options))
            {
                repo = new PositionCRUDRepo(context);
                result = await repo.DeletePosition(1);
            }

            //Assert
            Assert.Null(result);
        }
    }
}
