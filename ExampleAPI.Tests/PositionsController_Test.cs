﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using ExampleAPI.Controllers;
using ExampleAPI.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using ExampleAPI.Interfaces;
using Microsoft.Extensions.Logging;
using ExampleAPI.DTOs;

namespace ExampleAPI.Tests
{

    public class PositionsController_Test
    {

        public PositionsController_Test()
        {

        }
        //Task<List<Position>> GetPositions();
        public Task<List<Position>> TestEmptyPositionList()
        {
            return Task.FromResult(new List<Position>());
        }
        public Task<List<Position>> TestThreePositionList()
        {

            return Task.FromResult<List<Position>>(new List<Position> {
                new Position{Id = 1 },
                new Position{Id = 2 },
                new Position{Id = 3}
            });
        }
        //Task<Position> GetPosition(int id);
        public Task<Position> TestNullPosition()
        {
            return Task.FromResult<Position>(null);
        }
        public Task<Position> TestPositionId1()
        {
            return Task.FromResult(new Position { Id = 1 });
        }

        public Task<List<SecType_AveragePrice>> TestEmptySecType_AveragePrice()
        {
            return Task.FromResult(new List<SecType_AveragePrice>());
        }
        public Task<List<SecType_AveragePrice>> TestSecType_AveragePrice()
        {

            return Task.FromResult(new List<SecType_AveragePrice> {
                new SecType_AveragePrice{SecType = "a"},
                new SecType_AveragePrice{SecType = ""},
                new SecType_AveragePrice{SecType = null},
                new SecType_AveragePrice{SecType = "b", AveragePrice = 0}
            });
        }

        //GetPositions return none if empty
        [Fact]
        public async Task GetPositions_ReturnsOK_IfEmpty()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.GetPositions()).Returns(TestEmptyPositionList());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPositions();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            var returnValue = Assert.IsType<List<Position>>(actionResult.Value);
            Assert.Empty(returnValue);
        }

        //GetPositions return all if full
        [Fact]
        public async Task GetPositions_ReturnsAViewResult_WithAListOfPositions()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.GetPositions()).Returns(TestThreePositionList());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPositions();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            var returnValue = Assert.IsType<List<Position>>(actionResult.Value);
            Assert.Equal(3, returnValue.Count);
        }

        //GetPositions return badrequest on exception
        [Fact]
        public async Task GetPositions_OnException_ReturnsBadRequest()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.GetPositions()).Throws(new Exception());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPositions();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            Assert.IsType<BadRequestResult>(actionResult.Result);
        }

        //GetPosition return notfound
        [Fact]
        public async Task GetPosition_ReturnsNotFound()
        {
            int input = 0;
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.GetPosition(input)).Returns(TestNullPosition());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            Assert.IsType<NotFoundResult>(actionResult.Result);
        }

        //GetPosition return position
        [Fact]
        public async Task GetPosition_ReturnsPosition()
        {
            int input = 1;
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.GetPosition(input)).Returns(TestPositionId1());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            Assert.NotNull(result.Value);
        }

        //GetPosition return badrequest on exception
        [Fact]
        public async Task GetPosition_OnException_ReturnsBadRequest()
        {
            int input = 1;
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.GetPosition(input)).Throws(new Exception());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            Assert.IsType<BadRequestResult>(actionResult.Result);
        }

        //PutPosition not found
        [Fact]
        public async Task PutPosition_CallsUpdate_ReturnsNotFound()
        {
            Position input = new Position();
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.UpdatePosition(input)).Returns(Task.FromResult(0)).Verifiable();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.PutPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            mockCRUDRepo.Verify();
            Assert.IsType<NotFoundResult>(actionResult.Result);
        }

        //PutPosition successful updated
        [Fact]
        public async Task PutPosition_CallsUpdate_ReturnsPosition()
        {
            Position input = new Position();
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.UpdatePosition(input)).Returns(Task.FromResult(1)).Verifiable();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.PutPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            mockCRUDRepo.Verify();
            Assert.NotNull(result.Value);
        }

        //PutPosition exception thrown
        [Fact]
        public async Task PutPosition_OnException_ReturnsBadRequest()
        {
            Position input = new Position();
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.UpdatePosition(input)).Throws(new Exception());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.PutPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            Assert.IsType<BadRequestResult>(actionResult.Result);
        }

        //PostPosition position already exists
        [Fact]
        public async Task PostPosition_AlreadyExists_ReturnsUnprocessable()
        {
            Position input = new Position { Id = 1 };
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.CreatePosition(input)).Returns(Task.FromResult(0)).Verifiable();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.PostPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            mockCRUDRepo.Verify();
            Assert.IsType<UnprocessableEntityResult>(actionResult.Result);
        }

        //PostPosition successful create
        [Fact]
        public async Task PostPosition_CallsCreate_ReturnsPosition()
        {
            Position input = new Position { Id = 1 };
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.CreatePosition(input)).Returns(Task.FromResult(1)).Verifiable();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.PostPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            var createdAtActionResult = Assert.IsType<CreatedAtActionResult>(actionResult.Result);
            var returnValue = Assert.IsType<Position>(createdAtActionResult.Value);
            mockCRUDRepo.Verify();
            Assert.NotNull(returnValue);
        }

        //PostPosition bad request on exception
        [Fact]
        public async Task PostPosition_OnException_ReturnsBadRequest()
        {
            Position input = new Position { Id = 1 };
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.CreatePosition(input)).Throws(new Exception());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.PostPosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            Assert.IsType<BadRequestResult>(actionResult.Result);
        }

        //DeletePosition notfound
        [Fact]
        public async Task DeletePosition_NotFound()
        {
            int input = 1;
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.DeletePosition(input)).Returns(Task.FromResult<Position>(null));
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.DeletePosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            mockCRUDRepo.Verify();
            Assert.IsType<NotFoundResult>(actionResult.Result);
        }

        //DeletePosition deletes
        [Fact]
        public async Task DeletePosition_CallsDelete_ReturnsPosition()
        {
            int input = 1;
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.DeletePosition(input)).Returns(TestPositionId1());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.DeletePosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            mockCRUDRepo.Verify();
            Assert.NotNull(result.Value);
        }

        //DeletePosition on exception returns badrequest
        [Fact]
        public async Task DeletePosition_OnException_ReturnsBadRequest()
        {
            int input = 1;
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            mockCRUDRepo.Setup(repo => repo.DeletePosition(input)).Throws(new Exception());
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.DeletePosition(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<Position>>(result);
            Assert.IsType<BadRequestResult>(actionResult.Result);
        }

        //GetAveragePriceBySecType return empty
        [Fact]
        public async Task GetAveragePriceBySecType_ReturnsOK_IfEmpty()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetAveragePriceBySecType()).Returns(TestEmptySecType_AveragePrice());
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetAveragePriceBySecType();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<SecType_AveragePrice>>>(result);
            var returnValue = Assert.IsType<List<SecType_AveragePrice>>(actionResult.Value);
            Assert.Empty(returnValue);
        }

        //GetAveragePriceBySecType return successful
        [Fact]
        public async Task GetAveragePriceBySecType_ReturnsList()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetAveragePriceBySecType()).Returns(TestSecType_AveragePrice()).Verifiable();
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetAveragePriceBySecType();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<SecType_AveragePrice>>>(result);
            var returnValue = Assert.IsType<List<SecType_AveragePrice>>(actionResult.Value);
            mockDetailRepo.Verify();
            Assert.NotEmpty(returnValue);
        }

        //GetAveragePriceBySecType return badrequest on exception
        [Fact]
        public async Task GetAveragePriceBySecType_OnException_ReturnsBadRequest()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetAveragePriceBySecType()).Throws(new Exception());
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetAveragePriceBySecType();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<SecType_AveragePrice>>>(result);
            Assert.IsType<BadRequestResult>(actionResult.Result);
        }

        //GetNonInvestmentGradePositions return ok if empty
        [Fact]
        public async Task GetNonInvestmentGradePositions_ReturnsOK_IfEmpty()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetNonInvestmentGradePositions()).Returns(TestEmptyPositionList());
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetNonInvestmentGradePositions();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            var returnValue = Assert.IsType<List<Position>>(actionResult.Value);
            Assert.Empty(returnValue);
        }

        //GetNonInvestmentGradePositions return list if not empty
        [Fact]
        public async Task GetNonInvestmentGradePositions_ReturnsAViewResult_WithAListOfPositions()
        {
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetNonInvestmentGradePositions()).Returns(TestThreePositionList());
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetNonInvestmentGradePositions();

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            var returnValue = Assert.IsType<List<Position>>(actionResult.Value);
            Assert.Equal(3, returnValue.Count);
        }

        //GetPositionsByRiskCountry return empty
        [Fact]
        public async Task GetPositionsByRiskCountry_ReturnsOK_IfEmpty()
        {
            string input = "a";
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetPositionsByRiskCountry(input)).Returns(TestEmptyPositionList());
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPositionsByRiskCountry(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            var returnValue = Assert.IsType<List<Position>>(actionResult.Value);
            Assert.Empty(returnValue);
        }

        //GetPositionsByRiskCountry return list if not empty
        [Fact]
        public async Task GetPositionsByRiskCountry_ReturnsAViewResult_WithAListOfPositions()
        {
            string input = "a";
            // Arrange
            var mockCRUDRepo = new Mock<IPositionCRUDRepo>();
            var mockDetailRepo = new Mock<IPositionDetailRepo>();
            mockDetailRepo.Setup(repo => repo.GetPositionsByRiskCountry(input)).Returns(TestThreePositionList());
            var mockLogger = new Mock<ILogger<PositionsController>>();
            var controller = new PositionsController(mockCRUDRepo.Object, mockDetailRepo.Object, mockLogger.Object);

            // Act
            var result = await controller.GetPositionsByRiskCountry(input);

            // Assert
            var actionResult = Assert.IsType<ActionResult<List<Position>>>(result);
            var returnValue = Assert.IsType<List<Position>>(actionResult.Value);
            Assert.Equal(3, returnValue.Count);
        }
        
    }
}
