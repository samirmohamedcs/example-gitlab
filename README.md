This is a test project to demonstrate a .Net Core/Entity Framework API with CI/CD using Gitlab.

**Framework/Libraries:**

* .Net Core 2.2
* Entity Framework Core
* xunit




**Current Functionality:**

Perform CRUD and other queries through an API on Position.

Position object
```
[SecName] [varchar](200) NOT NULL,
[SecType] [varchar](10) NULL,
[Issuer] [varchar](200) NULL,
[CreatedAt] [datetime] NOT NULL,
[CCY] [varchar](300) NULL,
[Ticker] [varchar](20) NULL,
[PriceMultiplier] [decimal](28, 4) NULL,
[IssuePrice] [decimal](28, 4) NULL,
[LastPrice] [decimal](28, 6) NULL,
[RatingSP] [varchar](10) NULL,
[RatingMoodys] [varchar](10) NULL,
[CUSIP] [varchar](20) NULL,
[AssetType] [varchar](100) NULL,
[RiskCountry] [varchar](300) NULL,
[ID] [int] IDENTITY(1,1) NOT NULL primary key
```



**Endpoints:**

* get https://localhost:44397/api/Positions
* get https://localhost:44397/api/Positions/{id}
* put https://localhost:44397/api/Positions  
* post https://localhost:44397/api/Positions
* delete https://localhost:44397/api/Positions/{id}
* get https://localhost:44397/api/Positions/GetAveragePriceBySecType
* get https://localhost:44397/api/Positions/GetNonInvestmentGradePositions
* get https://localhost:44397/api/Positions/GetPositionsByRiskCountry/{country}



**Design:**

Models are created through ef core. Ef core dbcontexts are injected into repositories.
Repositories are used by controllers through DI. Unit tests are done on controllers by
mocking the repos. Unit tests are done on repos by creating a dbcontext that uses
an in memory sql lite database. Gitlab is configured to restore nuget packages, build,
then run tests on commit.

**Sql source control considerations:**

SSDT is a popular tool, free with visual studio license, and uses state based tracking.
It looks to be a good tool but one big consideration is that it requires the full .Net
Framework, not just .net core. This means it also requires windows which would need
to be considered for ci/cd. 

EF Core migrations would not require additional dependencies because the project
already utilizes EF Core. It uses migration based tracking, which can be more painful
when trying to merge changes in source control.